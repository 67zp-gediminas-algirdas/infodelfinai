# InfoDelfinai
InfoDelfinai yra naujienu portalas parašytas ant Laravel sistemos.

## Paruošta duomenų bazė
Prisijungimas: admin@admin.com 1kO2LCU4W5zp

## Programavo
- Algirdas Butkus 67ŽP18MOD
- Gediminas Bajorinas 67ŽP18MOD

## Naudota
- Laravel
- Bootstrap
- MySQL
- InstantClick

## Kaip gauti administratoriaus teises
Per PHPMyAdmin eiti į "users" ir nustatyti "role" į "superadmin" arba "admin"

## Straipsnių sintaksė
- #h1 Labai didelis tekstas
- #h2 Didelis tekstas
- #h3 Didesnis nei vidutinis tekstasaa
- #i Pasvirasis šriftas
- #b Paryškintas šriftas

- #imgabs \<Nuotraukos URL\>
- #img \<Nuotrauka kuri yra pačioje svetainėje\>

- #hr Linija

- #linkurl nuoroda
- #linktxt nuorodos tekstas

## Straipsnio pavyzdys:
```
Jonas Jonevičius atidarė savo naujienų portalą! Vieni sakė kad jam nepavyks to padaryti, bet jis įrodė kad tai įmanoma!

Rašo Benas Benevičius:
#i Nesitikėjau kad jo portalas išpopulerės!

Galite apsilankyti jo naujienu portale čia:
#linkurl https://laravel-news.com/
#linktxt Laravel
```

## Nuotraukos
![1](https://cdn.discordapp.com/attachments/493500446573461525/712020827893399673/unknown.png)
![2](https://cdn.discordapp.com/attachments/493500446573461525/712020867705602078/unknown.png)
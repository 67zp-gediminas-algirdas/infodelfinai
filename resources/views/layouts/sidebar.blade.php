<aside>
<h3>Kategorijos</h3>
@forelse ($categories ?? [] as $cat)
<a class="d-block" href="{{asset('kateg/'.$cat->url)}}">{{ $cat->name }}</a>
@empty
Tuščia
@endforelse
<br>
<h3>Kita</h3>
<a class="d-block" href="{{asset('/')}}">Naujausia</a>
<a class="d-block" target="_blank" rel="noopener noreferrer" href="https://gitlab.com/67zp-gediminas-algirdas/infodelfinai/">GitLab</a>
<br>
<h3>Programavo</h3>
<span class="dim-text">67ŽP18MOD<br>Algirdas Butkus<br>Gediminas Bajorinas</span>
<br>
<br>
<h3>Panaudota</h3>
<span class="dim-text">Laravel<br>
MySQL<br>
Google Fonts<br>
Bootstrap<br>
InstantClick<br>
Colcade</span>



</aside>
<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <!-- <script  data-no-instantsrc="{{ asset('js/grid.js') }}" defer></script> -->

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css2?family=Ubuntu:wght@300&display=swap" rel="stylesheet"> 

    <!-- Styles -->
    <link href="{{ asset('css/bootstrap.css') }}" rel="stylesheet">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-expand navbar-light">
            <div class="container">

                <a href="{{ url('/') }}"><img class="navbar-brand" height="50" src="{{ url('/img/logo.svg') }}"></a>


                    <!-- Left Side Of Navbar -->
                    <ul class="mr-auto navbar-nav">

                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="ml-auto navbar-nav">
                        <!-- Authentication Links -->
                        @guest
                        @else
                        <li class="nav-item">
                                <a class="nav-link username">
                                    {{ Auth::user()->name }}
                                </a>
                            </li>
                        @endguest
                        <li class="nav-item">
                            <a class="nav-link" href="{{ url('/') }}">Namai</a>
                        </li>
                        @guest
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}">Prisijungti</a>
                            </li>
                            @if (Route::has('register'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('register') }}">Registruotis</a>
                                </li>
                            @endif
                        @else
                            @if(Auth::user()->role('admin')||Auth::user()->role('superadmin')||Auth::user()->role('author'))
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('article-settings') }}">Straipsnių valdymas</a>
                            </li>
                            @endif
                            @if(Auth::user()->role('admin')||Auth::user()->role('superadmin'))
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('category-settings') }}">Kategorijų valdymas</a>
                            </li>
                            @endif

                            <li class="nav-item">
                                <form id="logout-form" action="{{ route('logout') }}" method="POST">
                                    @csrf
                                    <input class="nav-link rmcss danger-text" type="submit" value="Atsijungti">
                                </form>
                            </li>
                        @endguest
                    </ul>
            </div>
        </nav>

        <main class="py-4">
            @yield('content')
        </main>

        <footer>
            <div class="centras row">
                <div class="col-12 col-sm-6">
                    2020-05-18
                    <br>
                    Algirdas Butkus
                    <br>
                    Gediminas Bajorinas
                </div>
                <div class="col-12 col-sm-6">
                    <img class="navbar-brand" height="50" src="{{ url('/img/logo.svg') }}">
                    <br>
                    Visos teisės nėra saugomos, 2020.
                </div>
            </div>
        </footer>
    </div>
    <script src="{{ asset('js/instantclick.min.js') }}" data-no-instant></script>
    <script data-no-instant>InstantClick.init();</script>
    <script src="https://unpkg.com/colcade@0/colcade.js" data-no-instant></script>
    <script>
        var colc = new Colcade( '.rgrid', {
            columns: '.rgrid-col',
            items: '.rgrid-item'
        });
    </script>
</body>
</html>

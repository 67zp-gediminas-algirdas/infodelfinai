@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        @if(isset($editcat))
            <div class="pb-4 col-12">
                <div class="card">
                    <div class="card-header">Redaguoti "{{$editcat->title}}" straipsni</div>

                    <div class="card-body">
                        <form method="POST" enctype="multipart/form-data" action="{{ route('article-exec-modify') }}">
                            <input type="hidden" name="id" value="{{ $editcat->id }}">
                            @csrf

                            <div class="form-group row">
                                <label for="name" class="col-md-4 col-form-label text-md-right">Pavadinimas</label>

                                <div class="col-md-6">
                                    <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{$editcat->title}}" required autocomplete="name" autofocus>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-md-4 col-form-label text-md-right">Trumpas aprašymas</label>

                                <div class="col-md-6">
                                    <input id="desc" type="text" class="form-control" name="desc" value="{{$editcat->shortDescr}}">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-md-4 col-form-label text-md-right">URL</label>

                                <div class="col-md-6">
                                    <input id="url" type="text" class="form-control" name="url" value="{{$editcat->url}}">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-md-4 col-form-label text-md-right">Kategorija</label>
                                <div class="col-md-6">
                                    
                                    @forelse ($categories as $cat)
                                        <div class="form-check">
                                            <input class="form-check-input" type="radio" name="category" id="category" value="{{$cat->id}}" {{ ($cat->id==$editcat->category)?'checked':'' }}>
                                            <label class="form-check-label" for="category">
                                                {{$cat->name}}
                                            </label>
                                        </div>
                                    @empty
                                        <p class="mb-4">Nėra kategoriju!<p>
                                    @endforelse
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-md-4 col-form-label text-md-right">Nuotrauka</label>

                                <div class="col-md-6">
                                    <img class="img" style="max-height: 150px" src="{{ $editcat->thumbnail }}">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-md-4 col-form-label text-md-right"></label>

                                <div class="col-md-6">
                                    <input type="file" class="form-control-file" id="image" name="image" accept="image/*">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-md-4 col-form-label text-md-right">Straipsnis</label>

                                <div class="col-md-6">
                                    <textarea rows="7" id="straipsnis" type="text" class="form-control" name="straipsnis">{{$editcat->content}}</textarea>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-md-4 col-form-label text-md-right"></label>

                                <div class="col-md-6">
                                    <div class="form-check">
                                        <input type="checkbox" autocomplete="off" name="public" {{$editcat->public ? 'checked' : ''}} class="form-check-input" id="exampleCheck1">
                                        <label class="form-check-label">Matomas/viešas</label>
                                    </div>
                                </div>
                            </div>

                            <div class="mb-0 form-group row">
                                <div class="col-md-6 offset-md-4">
                                    <button type="submit" class="btn btn-primary">Keisti</button>
                                    <a class="btn btn-secondary" href="{{route('article-settings')}}">Atšaukti</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        @endif
        <div class="pb-4 col-12 col-xl-6">
            <div class="card">
                <div class="card-header">Pridėti straipsni</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('article-add') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">Pavadinimas</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-md-right">Trumpas aprašymas</label>

                            <div class="col-md-6">
                                <input id="desc" type="text" class="form-control" name="desc">
                            </div>
                        </div>

                        <div class="mb-0 form-group row">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">Pridėti</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="pb-4 col-12 col-xl-6">
            <div class="card">
                <div class="card-header">Redaguoti straipsnius</div>

                <div class="card-body pb-0">
                    @forelse ($articles as $cat)
                        <div class="card mb-4">
                            <p class="card-header-2">{{ $cat->title }}</p>
                            <div class="row m-2 mb-4 mt-4">
                                <div class="col-12 col-md-6">
                                    <p class="mb-0">Aprašymas: <b>{{ $cat->shortDescr }}</b></p>
                                    <p class="mb-0">Kategorija: <b>{{ $cat->categoryName }}</b></p>
                                    <p class="mb-0">Autorius: <b>{{ $cat->name }}</b></p>
                                    <p class="mb-0">Paviešinta: 
                                        @if($cat->public)
                                        <b class="text-success">Taip</b>
                                        @else
                                        <b class="text-danger">Ne</b>
                                        @endif
                                    </p>
                                    <p class="mb-0">URL: <b>{{ $cat->url }}</b></p>
                                </div>
                                <div class="col-12 col-md-6 text-right">
                                    <img class="img" style="max-height: 150px" src="{{ $cat->thumbnail }}">
                                    <a class="btn btn-primary" href="{{route('article-modify',['id'=>$cat->id])}}">Redaguoti</a>
                                    <a class="btn btn-primary" href="{{asset('/s/'.$cat->url)}}">Peržiurėti</a>
                                    <form class="d-inline-block mt-1 mb-1" method="POST" action="{{ route('article-delete') }}"
                                        onsubmit="return confirm('Ar tikrai norite ištrinti {{$cat->url}} straipsni?')">
                                        @csrf
                                        <input type="hidden" name="id" value="{{ $cat->id }}">
                                        <button type="submit" class="btn btn-danger">Trinti</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    @empty
                        <p class="mb-4">Tuščia!<p>
                    @endforelse
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        @if(isset($editcat))
            <div class="pb-4 col-12">
                <div class="card">
                    <div class="card-header">Redaguoti "{{$editcat->name}}" kategorija</div>

                    <div class="card-body">
                        <form method="POST" action="{{ route('category-exec-modify') }}">
                            <input type="hidden" name="id" value="{{ $editcat->id }}">
                            @csrf

                            <div class="form-group row">
                                <label for="name" class="col-md-4 col-form-label text-md-right">Pavadinimas</label>

                                <div class="col-md-6">
                                    <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{$editcat->name}}" required autocomplete="name" autofocus>

                                    @error('name')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-md-4 col-form-label text-md-right">Trumpas aprašymas</label>

                                <div class="col-md-6">
                                    <input id="desc" type="text" class="form-control" name="desc" value="{{$editcat->description}}">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-md-4 col-form-label text-md-right">URL</label>

                                <div class="col-md-6">
                                    <input id="url" type="text" class="form-control" name="url" value="{{$editcat->url}}">
                                </div>
                            </div>

                            <div class="mb-0 form-group row">
                                <div class="col-md-6 offset-md-4">
                                    <button type="submit" class="btn btn-primary">Keisti</button>
                                    <a class="btn btn-secondary" href="{{route('category-settings')}}">Atšaukti</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        @endif
        <div class="pb-4 col-12 col-lg-6">
            <div class="card">
                <div class="card-header">Pridėti kategorija</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('category-add') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">Pavadinimas</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-md-right">Trumpas aprašymas</label>

                            <div class="col-md-6">
                                <input id="desc" type="text" class="form-control" name="desc">
                            </div>
                        </div>

                        <div class="mb-0 form-group row">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">Pridėti</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="pb-4 col-12 col-lg-6">
            <div class="card">
                <div class="card-header">Redaguoti kategorijas</div>

                <div class="card-body pb-0">
                    @forelse ($categories as $cat)
                        <div class="card mb-4">
                            <p class="card-header-2">{{ $cat->name }}</p>
                            <div class="row m-2 mb-4 mt-4">
                                <div class="col-12 col-md-6">
                                    <p class="mb-0">Aprašymas: <b>{{ $cat->description }}</b></p>
                                    <p class="mb-0">URL: <b>{{ $cat->url }}</b></p>
                                </div>
                                <div class="col-12 col-md-6 text-right">
                                    <a class="btn btn-primary" href="{{route('category-modify',['id'=>$cat->id])}}">Redaguoti</a>
                                    <a class="btn btn-primary" href="{{asset('/kateg/'.$cat->url)}}">Peržiurėti</a>
                                    <form class="d-inline-block mt-1 mb-1" method="POST" action="{{ route('category-delete') }}"
                                        onsubmit="return confirm('Ar tikrai norite ištrinti {{$cat->url}} kategorija?')">
                                        @csrf
                                        <input type="hidden" name="id" value="{{ $cat->id }}">
                                        <button type="submit" class="btn btn-danger">Trinti</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    @empty
                        <p class="mb-4">Nėra kategoriju!<p>
                    @endforelse
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

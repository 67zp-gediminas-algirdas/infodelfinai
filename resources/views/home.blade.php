@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="pb-4 col-12 col-md-3 order-md-2">
            @include('layouts.sidebar')
        </div>
        <div class="pb-4 col-12 col-md-9 order-md-1">
            <h3>Naujausia</h3>
            {!! $list !!}
        </div>
    </div>
</div>
@endsection

@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="pb-4 col-12 col-md-3 order-md-2">
            @include('layouts.sidebar')
        </div>
        <div class="pb-4 col-12 col-md-9 order-md-1">
            @if($imgUrl!=NULL)
                <img src="{{$imgUrl}}" class="img">
                <br><br>
            @endif
            <p class="h2">{{ $current->title }}</p>
            <p class="h4">{{$current->shortDescr}}</p>
            <p class="h6">{{$currentAuthor}} <small class="text-muted">{{$time}}</small></p>
            <br>
            <p>{!! $straipsnis !!}</p>
            <hr>
            <h3 class="centras">Komentarai</h3>
            
<div class="card card-komentaras">
<div class="card-header"><b>Benas</b> 2020-05-02 16:51</div>
    <div class="card-body">
<button type="button" class="btn btn-green btn-above px-3">
    <svg class="bi bi-heart-fill" width="1.5em" height="1.5em" viewBox="0 0 16 16" fill="#FFFFFF" xmlns="http://www.w3.org/2000/svg">
        <path fill-rule="evenodd" d="M8 1.314C12.438-3.248 23.534 4.735 8 15-7.534 4.736 3.562-3.248 8 1.314z" clip-rule="evenodd"/>
    </svg>
</button>
<p class="card-tekstas">Geras!</p>
        </div>
</div>


<div class="card card-komentaras">
<div class="card-header"><b>Jonas</b> 2020-05-01 21:07</div>
    <div class="card-body">
<button type="button" class="btn btn-green btn-above px-3">
    <svg class="bi bi-heart-fill" width="1.5em" height="1.5em" viewBox="0 0 16 16" fill="#FFFFFF" xmlns="http://www.w3.org/2000/svg">
        <path fill-rule="evenodd" d="M8 1.314C12.438-3.248 23.534 4.735 8 15-7.534 4.736 3.562-3.248 8 1.314z" clip-rule="evenodd"/>
    </svg>
</button>
<p class="card-tekstas">Oho! Nesitikėjau visiškai to ką perskaičiau! Pribloškė visiškai.</p>
        </div>
</div>



<div class="card card-komentaras">
<div class="card-header"><b>Vardas</b> Laikas</div>
    <div class="card-body">
<button type="button" class="btn btn-green btn-above px-3">
    <svg class="bi bi-heart-fill" width="1.5em" height="1.5em" viewBox="0 0 16 16" fill="#FFFFFF" xmlns="http://www.w3.org/2000/svg">
        <path fill-rule="evenodd" d="M8 1.314C12.438-3.248 23.534 4.735 8 15-7.534 4.736 3.562-3.248 8 1.314z" clip-rule="evenodd"/>
    </svg>
</button>
<p class="card-tekstas">Komentarai kol kas neveikia</p>
        </div>
</div>


        </div>
    </div>
</div>
@endsection


<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'CategoryController@showPopular', function () {
    return view('home');
})->name('home');

Route::get('/kateg/{kategorija}', 'CategoryController@showCategory', function () {
})->name('category');

Auth::routes();

Route::get('/s/{straipsnis}', 'ArticleController@showArticle', function () {
})->name('article');


Route::middleware('can:accessAuthor')->group(function() {

    Route::get('/straipsnio-valdymas', 'ArticleController@settings', function () {
        return view('article-settings');
    })->name('article-settings');



    Route::get('/straipsnio-valdymas/keisti/{id}', 'ArticleController@settings', function () {
        return view('article-modify');
    })->name('article-modify');

    Route::post('/straipsnio-valdymas/keisti', 'ArticleController@updateArticle', function () {
    })->name('article-exec-modify');

    Route::post('/straipsnio-valdymas/prideti', 'ArticleController@addArticle', function () {
    })->name('article-add');

    Route::post('/straipsnio-valdymas/naikinti', 'ArticleController@deleteArticle', function () {
    })->name('article-delete');
    
});
Route::middleware('can:accessAdminpanel')->group(function() {

    Route::get('/kategoriju-valdymas', 'CategoryController@settings', function () {
        return view('category-settings');
    })->name('category-settings');



    Route::get('/kategoriju-valdymas/keisti/{id}', 'CategoryController@settings', function () {
        return view('category-modify');
    })->name('category-modify');

    Route::post('/kategoriju-valdymas/keisti', 'CategoryController@updateCategory', function () {
    })->name('category-exec-modify');

    Route::post('/kategoriju-valdymas/prideti', 'CategoryController@addCategory', function () {
    })->name('category-add');

    Route::post('/kategoriju-valdymas/naikinti', 'CategoryController@deleteCategory', function () {
    })->name('category-delete');

});

Route::get('/home', function(){
    return redirect('/');
})->name('home');
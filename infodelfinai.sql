-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: May 19, 2020 at 01:13 AM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `infodelfinai`
--

-- --------------------------------------------------------

--
-- Table structure for table `articles`
--

CREATE TABLE `articles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `url` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `shortDescr` text COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `author` int(11) NOT NULL,
  `category` int(11) NOT NULL,
  `public` tinyint(1) NOT NULL,
  `thumbnail` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `articles`
--

INSERT INTO `articles` (`id`, `title`, `url`, `shortDescr`, `content`, `author`, `category`, `public`, `thumbnail`, `created_at`, `updated_at`) VALUES
(4, 'Coronavirusas plinta lietuvoje!', 'coronavirusas-plinta-lietuvoje', 'Iššūkis medikams: 15 naujų protrūkių, visi panikoje', 'Ukrainoje per praėjusią parą užregistruota dar 455 užsikrėtimo koronavirusine infekcija COVID-19 atvejai ir 11 naujų mirčių, o 175 pacientai pasveiko, penktadienį pranešė Koronaviruso epidemijos plitimo stebėsenos sistemos tinklalapis.\r\n\r\nRyte paskelbti tinklalapio duomenys rodė, kad Ukrainoje šiuo metu yra 9 176 sergantys COVID-19 asmenys – 296 daugiau nei ketvirtadienį.\r\n\r\nKetvirtadienį buvo pranešta apie 450 naujai susirgusius žmones ir 11 aukų, taip pat apie 135 pasveikusius asmenis.\r\n\r\nEpidemiologinė padėtis Ukrainoje leidžia pradėti švelninti karantiną jau po pusantros savaitės, pareiškė sveikatos apsaugos ministras Maksymas Stepanovas.\r\n\r\n„Nuo gegužės 11-osios imsimės švelninimo, nes šiandien tai leidžia padėtis. Šis švelninimas bus grindžiamas turimais duomenimis. Esame įsitikinę, kad ateityje mūsų veiksmai nepablogins situacijos“, – pareiškė jis per spaudos konferenciją Čerkasuose.\r\n\r\n„Suprantame, kad žmonės pavargo nuo karantino. Noriu dar kartą pabrėžti, kad analizuojame padėti ir ruošiamės švelninti karantinavimo priemones jau nuo gegužės 11 dienos“, – nurodė ministras.', 1, 4, 1, 'https://kauno.diena.lt/sites/default/files/styles/940x000/public/Vilniausdiena/Vartotoju%20zona/eglmor/92518385_3407742115906003_9158881213119725568_n_1.jpg?itok=bu3Cwdpz', 1589745049, 1589745049),
(5, 'Oksfordas jau testuoja vakcina nuo COVID-19', 'oksfordas-jau-testuoja-vakcina-nuo-covid19', 'Tyrimai vyksta, ar pasiseks jie sužinosime greitu metu', 'Oksfordo universitetas pradeda klinikinius potencialios vakcinos nuo COVID-19 tyrimus. Tyrėjai viliasi, kad skiepai plačiajai visuomenei bus prieinami dar šiemet.\r\n\r\nKaip anksčiau skelbė Jungtinės Tautos (JT), sugrįžti į įprastą gyvenimą galėsime tik tada, kai bus išrasta veikianti vakcina. Iš daugiau nei 100 projektų visame pasaulyje septynios komandos jau atlieka klinikinius tyrimus, rodo Londono higienos ir atogrąžų medicinos mokyklos duomenys.\r\n\r\nTokie tyrimai jau vykdomi Kinijoje ir JAV, o Vokietijoje, trečiadienį šalies valdžiai davus leidimą, klinikiniai tyrimai turėtų prasidėti šio mėnesio pabaigoje.\r\n\r\nJungtinės Karalystės (JK) vyriausybė stipriai palaiko Oksfordo universiteto tyrėjų darbą, ir klinikiniai tyrimai su žmonėmis šalyje turėtų prasidėti ketvirtadienį, teigė JK sveikatos apsaugos sekretorius Mattas Hancockas. Jis džiaugėsi daug žadančių tyrimų eiga ir pabrėžė, kad pasiekti tokį tyrimų etapą, koks jau pasiektas dabar, dažniausiai prireikia bent kelerių metų.\r\n\r\nPirmuoju tyrimų etapu pusei visų 1 112 savanorių bus suleista potenciali vakcina prieš COVID-19, likusiems žmonėms bus suleista kontrolinė vakcina, kad būtų patikrintas eksperimentinės vakcinos saugumas ir efektyvumas.\r\n\r\nSavanoriai yra 18–55 metų amžiaus, stiprios sveikatos, neserga COVID-19, nesilaukia, nemaitina krūtimi.\r\n\r\nDešimt žmonių eksperimentinė vakcina bus suleista iš viso du kartus, tarp dviejų skiepų bus keturių savaičių pertrauka.\r\n\r\nProfesorės Sarahos Gilbert komanda tikisi, kad imunitetą COVID-19 įgis bent 80 proc. paskiepytųjų. Tokiu atveju mokslininkai iki rugsėjo mėnesio žada pagaminti skiepų milijonui žmonių. Jei vakcina pasiteisintų, rudenį ji taptų prieinama plačiajai visuomenei.\r\n\r\nTačiau visos klinikinius tyrimus jau atliekančios komandos savo portaluose rašo, kad tyrimai vykdomi itin greitai ir ambicingai, dėl to paskelbtos darbotvarkės gali stipriai keistis. Vis dėlto vyriausybės vyriausiasis medicinos darbuotojas Chrisas Whitty\'as trečiadienį patikino, kad tikimybė skiepus turėti dar šiemet yra „neįtikėtinai maža“.\r\n\r\n„Jei žmonės tikisi, kad vos atšaukę karantiną grįšime prie to, kas buvo įprasta, tai tėra realybės neatitinkantys lūkesčiai“, – sakė jis.', 1, 4, 1, 'https://kauno.diena.lt/sites/default/files/styles/940x000/public/Vilniausdiena/Vartotoju%20zona/eglmor/afp_1ri80t.jpg?itok=HKoKzhM8', 1589745489, 1589745489),
(6, 'Garsų mokslas: visa matančios akustinės bangos', 'garsu-mokslas-visa-matancios-akustines-bangos', 'Kodėl Bermudų trikampyje praėjusiame šimtmetyje gan dažnai dingdavo lėktuvai, o pasiklydę laivai būdavo randami be žmonių?', 'Viena hipotezė aiškino, jog dėl to kalti požeminiai drebėjimai, sukeldavę žemo dažnio garso bangas – infragarsą, išvesdavusį laivų jūreivius iš proto.\r\n\r\nEksperimentai, veikiant žmones 17 hercų dažnio garsu, patvirtino, kad tyrimo dalyviai iš tiesų jaučia nerimą ar baimę. Spėjama, jog būtent tokio dažnio garsai sklinda vietose, kuriose vaidenasi. Kaip ten bebūtų, mus iš tiesų nuolat supa įvairiausi garsai arba, tiksliau sakant, iš įvairių šaltinių sklindančios garso bangos. Mes patys taip pat esame garsų šaltinis. Akustinės bangos padeda mums orientuotis pasaulyje, leidžia pamatyti tai, ko nemato akis. Jomis netgi diagnozuojamos arba gydomos ligos. \r\n\r\n#h3 Pagrindai – iš senovės graikų Akustika tiria garso bangų ypatumus ir jų taikymus, mechaninių bangų sklidimą dujose, skysčiuose ir kietuose kūnuose. Gautos žinios jau nuo seno taikomos architektūroje, hidrolokacijoje, medžiagotyroje, medicinoje ir kitur. Naujausiomis žiniomis, ultragarso generatoriai, implantuoti pavojingiausiu smegenų augliu – glioblastoma sergančio paciento smegenyse, padeda vaistams lengviau iš kraujo patekti į smegenis. \r\n\r\n#i „Akustika – labai senas mokslas. Dar senovės graikai kūrė savo amfiteatrus taip, kad net aukščiausioje eilėje sėdintys žiūrovai girdėtų, kas šnibždama arenoje. Mūsų sritis yra fizikinė akustika. Tyrinėjame fizikinius reiškinius įvairiose medžiagose, pasitelkdami garsus. Akustika yra garsų mokslas, bet tie garsai yra girdimi ir negirdimi“\r\n– sako Lietuvos mokslo premijos laureatas fizikas prof. Daumantas Čiplys.\r\n\r\nŽmogus girdi garsus maždaug iki keliolikos tūkstančių virpesių per sekundę, t.y. keliolika kilohercų. Tačiau mums svarbūs ir ausiai negirdimi garsai – tokie kaip ultragarsas. Medicinoje naudojamų garso bangų dažnis gali siekti nuo kelių iki dešimties milijonų virpesių per sekundę. „Fizikai naudoja labai aukšto dažnio bangas, kurios virpa šimtus milijonų kartų per sekundę. Nuo šimtų megahercų iki gigahercų. Tos bangos tokios trumpos, kad jomis galima „pačiupinėti“ medžiagoje esančius atomus, jonus, elektronus, šviesos kvantus“, – LRT \r\n\r\nTelevizijos laidai „Mokslo ekspresas“ tvirtina D. Čiplys. Tai reiškia, kad garso bangos medžiagotyroje gali būti naudojamos taip pat, kaip lazeriai. Tik lazeris skleidžia optinio diapazono elektromagnetinę spinduliuotę, o garso bangos yra mechaniniai virpesiai. „Negalima sakyti, kad vienas metodas geresnis už kitą. Tai – skirtingi metodai, jie turi skirtingas galimybes. Mūsų dalis darbų yra iš akustooptikos srities. Akustooptika – tai akustinių bangų ir šviesos bangų sąveika. Sujungiame lazerius su akustinėmis bangomis“, – pasakoja D. Čiplys. Tokios sinergijos ištakų reiktų ieškoti XIX amžiuje. Labai svarbus įvykis buvo pjezoelektrinio reiškinio atradimas 1880 metais. Jo autoriai – broliai Pierre`as ir Jacques`as Curie. Tai reiškia, jog spaudžiant medžiagą, joje susidaro elektriniai krūviai, t.y. gaminama \r\n\r\nelektra. Galimas ir atvirkščias efektas. Prie pjezoelektrinėmis savybėmis pasižyminčios medžiagos prijungus elektros įtampą, medžiaga deformuojasi. Ji pailgėja arba susitraukia. Žodžiu, keičia savo formą. „Vienas iš žinomų pjezoelektrinio reiškinio buityje pritaikymų – dujų uždegimo kibirkštėlės. Paspaudus mygtuką, susispaudžia viduje esantis kristaliukas ir susidaro tokia aukšta įtampa, kad šoka kibirkštis. Šis reiškinys būdingas ne visoms medžiagoms, o tik pjezoelektrikams. Tai leidžia generuoti aukšto dažnio aukustines bangas“, – aiškina D. Čiplys. 1885 metais anglų fizikas, Nobelio premijos laureatas Lordas Reilis atrado, jog žemės drebėjimų metu paviršiumi sklinda seisminės bangos. Tai laikoma paviršinių akustinių bangų tyrimo pradžia. VU fizikų naudojamos akustinės bangos yra analogiškos \r\n\r\nmechaninėms seisminėms bangoms, tik jų ilgiai daug mažesni. Vaizdžiai sakant, mokslininkai kristaluose sukelia mikrodrebėjimus ir taip sužadina paviršines akustines bangas. „Akustooptika – sąveika tarp šviesos ir akustinių bangų buvo realizuota XX a. pirmoje pusėje. Bet tai dar nebuvo audringas vystymasis, ypač paviršinių bangų, o daugiau buvo dirbama su tūrinėmis bangomis. Tuo metu vystėsi hidrolokacija aptikti laivams. Toliau pjezoelektrikai buvo naudojami radiotechnikoje. Revoliucingas pokytis įvyko praeito amžiaus antroje pusėje, kai buvo surastas metodas efektyviai ir nebrangiai žadinti paviršines akustines bangas“, – „Mokslo ekspresui“ pasakoja D. Čiplys. Buvo sukurti paviršinių akustinių bangų keitikliai. Juos sudaro dvi grupės elektrodų, sunertų vienas į kitą. Tai sukėlė revoliuciją elektroninių prietaisų gamyboje. Dabar sunku įsivaizduoti prietaisą, kuriame tokio įtaiso nebūtų. Pradedant mobiliuoju telefonu ir baigiant radijo aparatu arba televizoriumi. Toks keitiklis, paprastai kalbant, yra filtras. Juk radijo imtuvas, \r\n\r\ntelevizorius ar kitas įrenginys turi iš visų erdvėje sklindančių dažnių spektro išskirti tik tam tikrą dažnį, kurį jis priima. „Anksčiau radijo imtuvuose buvo ritė ir kondensatorius, kuriuos reikdavo sukioti. Tai buvo nelabai patogu. Paviršinis akustinių bangų įtaisas yra filtras, nes jis žadina akustines bangas tik tam tikro dažnio. Tie filtrai maži, nebrangūs ir efektyvūs“, – tvirtina D. Čiplys. Tokie filtrai, juos kiek modifikavus, gali tapti norimo dažnio garso bangų generatoriumi. Taip veikia jūsų garažo duris arba automobilio dureles atrakinantis pultelis, siunčiantis radijo bangas. Jas kuria paviršinių akustinių bangų generatorius. VU Fizikos fakultetoradiofizikos katedros fizikinės akustikos laboratorijos mokslininkai šių metų Lietuvos mokslo premiją pelnė už darbų ciklą „Aukštadažnės akustinės bangos feroelektriniuose kristaluose, plačiatarpiuose puslaidininkiuose ir nanostruktūriniuose dariniuose“. „Paviršinės bangos mus domino kitais aspektais. Kadangi paviršinė banga sklinda paviršiumi, ji labai jautri paviršiaus, kuriuo sklinda, savybėms. Todėl labai didelės perspektyvos panaudoti paviršines akustines bangas jutikliuose. Jutiklis – tai toks įtaisas, kuris jaučia kažkokį išorinį poveikį. Ar tai būtų temperatūra, ar dujų buvimas ore, drėgmė, ar spinduliuotė. Registruodami akustinės bangos reakciją, jūs galite pasakyti, koks buvo tas poveikis“, – pasakoja D. Čiplys. Taip veikia mokslininkų sukurtas kvėpavimo jutiklis, kuriame pagrindinį vaidmenį atlieka paviršinė akustinė banga. Kvėpuodamas \r\n\r\nžmogus išskiria tam tikrą drėgmės kiekį, kurį fiksuoja aparatūra. Kaip pavyko sukurti tokį jautrų ir spartų jutiklį? „Pirmiausia reikia parinkti medžiagas. Padėklas yra pjezoelektrinė plokštelė, kurioje sklinda paviršinė akustinė banga. Bet akustinės bangos kelyje yra ant paviršiaus uždedamas plonas drėgmei jautrus sluoksnis. Pasikeitus drėgmei, pasikeitė sluoksnio savybės ir akustinė banga tai užregistravo. Jeigu mes tokį jutiklį laikome arti nosies, užrašome, kaip žmogus kvėpuoja“, – LRT Televizijos laidai „Mokslo ekspresas“ pasakoja D. \r\n\r\nČiplys. Išskirtinis sukurtų jutiklių bruožas – labai spartus reagavimas į drėgmės pokyčius. Tai pavyko pasiekti, panaudojus šiuolaikines nanostruktūrines medžiagas – organinius porfirino junginius ir grafeno darinius. Pagal iškvepiamo oro sudėtį galima diagnozuoti įvairias ligas arba jų pradžią. Tokie jutikliai, juos tinkamai nukalibravus, galėtų tiksliai nustatyti, pavyzdžiui, acetono ar kitų patologiją liudijančių cheminių junginių kiekį.', 1, 3, 1, 'http://ffden-2.phys.uaf.edu/webproj/212_spring_2015/Dylan_Sanders/dylan_sanders/soundwaves.jpg', 1589745972, 1589745972),
(7, 'PSO: purkšti gatves dezinfekantais gali būti kenksminga', 'gatves-dezinfekantais-kenksminga', 'Kai kuriose šalyse atliekamas gatvių purškimas dezinfekantais nepadeda sunaikinti naujojo koronaviruso ir net kelia riziką sveikatai, perspėja Pasaulio sveikatos organizacija (PSO).', '#imgabs https://kauno.diena.lt/sites/default/files/styles/940x000/public/Vilniausdiena/Vartotoju%20zona/eglmor/92518385_3407742115906003_9158881213119725568_n_1.jpg?itok=bu3Cwdpz\r\n\r\nŠeštadienį paskelbtame dokumente dėl paviršių valymo ir dezinfekavimo kovojant su virusu PSO teigia, jog gatvių purškimas gali būti neefektyvus. „Siekiant sunaikinti COVID-19 virusą ar kitus patogenus, purkšti ar dezinfekuoti atvirų erdvių, tokių kaip gatvės ar turgavietės, ... nerekomenduojama, nes dezinfekantą gali neutralizuoti purvas ir apnašos, – paaiškino PSO. – Net ir nesant organinių medžiagų, mažai tikėtina, jog išpurkšti chemikalai padengs visus paviršius reikalingam kontakto laikotarpiui, kad būtų deaktyvuoti patogenai.“\r\n\r\n#imgabs https://kauno.diena.lt/sites/default/files/styles/940x000/public/Vilniausdiena/Vartotoju%20zona/eglmor/afp_1ri80t.jpg?itok=HKoKzhM8\r\n\r\nPSO pažymėjo, jog gatvės ir šaligatviai nėra laikomi COVID-19 „infekcijos rezervuarais“, ir pridūrė, jog chemikalų purškimas, net ir lauke, gali būti „pavojingas žmonių sveikatai“. Dokumente taip pat pabrėžiama, jog žmonių purškimas dezinfekantais „nėra rekomenduojamas jokiomis aplinkybėmis“. „Tai gali būti fiziškai ir psichologiškai kenksminga ir nesumažins užsikrėtusio žmogaus galimybės platinti virusą oro lašeliniu būdu ar kontakto metu“, – nurodoma dokumente. Jame pabrėžiama, jog žmonių purškimas chloru ar kitokiais toksiškais chemikalais gali dirginti akis ir odą, sukelti bronchų spazmų ir skrandžio bei žarnyno veiklos sutrikimų. Daugiau nei 300 tūkst. žmonių visame pasaulyje pražudęs virusas nusėda ant paviršių ir daiktų.\r\n\r\nTačiau šiuo metu neturima tikslios informacijos, kiek laiko virusas išlieka aktyvus ant paviršių. Atlikti tyrimai rodo, jog virusas ant tam tikrų paviršių išlieka kelias dienas. Tačiau šie maksimalūs laikotarpiai yra tik teoriniai, nes jie buvo užfiksuoti tik laboratorinėmis sąlygomis, todėl šios išvados turėtų būti „atsargiai aiškinamos“ ir taikomos realioje aplinkoje.', 1, 4, 1, 'https://www.wwno.org/sites/wwno/files/styles/x_large/public/202003/outbreak-coronavirus-world-1024x506px.jpg', 1589749375, 1589749375),
(8, 'Paskelbta COVID-19 statistika', 'paskelbta-covid19-statistika', 'Atnaujinta informacija apie situacija lietuvoje', '#h3 Per paskutinę parą šalyje nustatyti 6 koronaviruso atvejai, praneša Sveikatos apsaugos ministerija.\r\n\r\nPatvirtintų ligos atvejų skaičius konkretiems žmonėms:\r\n#b 1547 \r\n\r\nSergančių žmonių skaičius:\r\n#b 485\r\n\r\nPer vakar dieną patvirtintų naujų COVID 19 susirgusių žmonių skaičius:\r\n#b 6 \r\n\r\nMirusių nuo COVID 19 žmonių skaičius:\r\n#b 59 \r\n\r\nUžsikrėtusieji koronavirusu, mirę dėl kitų priežasčių:\r\n#b 6\r\n\r\nPasveikusių žmonių skaičius:\r\n#b 997\r\n\r\nIzoliacijoje esančių žmonių skaičius:\r\n#b 295\r\n\r\n\r\nSAM primena, kad nuo šiol dėl galimos infekcijos gali būti tiriami visi, kuriems pasireiškia ūmios kvėpavimo takų infekcijos simptomai. \r\n\r\nDešimtyje savivaldybių – Vilniaus, Kauno, Klaipėdos, Šiaulių, Panevėžio, Marijampolės, Tauragės, Telšių, Alytaus, Utenos – jau veikia mobilūs punktai, \r\nkuriuos pacientams imami ėminiai tyrimams dėl naujojo koronaviruso. \r\n\r\nSavarankiškai į mobilius punktus atvykę asmenys, neturintys SMS su koordinatoriaus nurodymais, nebus priimami.\r\n\r\nPrimename, kad pasitikrinti, ar nebuvote tose pačiose vietose, kaip ir koronavirusu užsikrėtę asmenys,\r\ngalite COVID-19 atvejų epidemiologinio tyrimo rezultatų lentelėje.\r\n\r\nTaip pat Lietuvos savivaldybėse duris netrukus turėtų atverti, o kai kuriose jau atvėrė vadinamosios karščiavimo klinikos, \r\nkuriose viršutinių kvėpavimo takų infekcijos simptomų turintiems pacientams bus atliekami išsamesni tyrimai, \r\nįtariant koronavirusinę infekciją. Į šias klinikas bus registruojami šeimos gydytojo siuntimą turintys pacientai, \r\nkurie turės atvykti jiems paskirtu konkrečiu laiku.\r\n\r\n#imgabs http://alkas.lt/wp-content/uploads/2020/03/koronavirusas-pexels-com-nuotr.jpeg\r\n\r\n#h3 Svarbiausia informacija apie koronavirusą\r\n\r\nKoronavirusas yra naujos struktūros virusas, kuris niekada anksčiau nebuvo sukėlęs infekcijų žmonėms. Pagrindiniai jo simptomai yra panašūs į gripo: karščiavimas,\r\nkosulys, apsunkęs kvėpavimas ir kiti kvėpavimo sutrikimai. Sunkesniais atvejais koronavirusai sukelia plaučių uždegimą, sunkų ūmų respiracinį sindromą, \r\ninkstų nepakankamumą ar mirtį.\r\n\r\nSpecifinio gydymo nuo naujojo koronaviruso sukeltos infekcijos nėra, taikomas tik simptominis gydymas. Susirgusieji gali būti visiškai išgydyti, \r\npriklausomai nuo jų sveikatos būklės bei nuo to, kada pradedamas taikyti gydymas.\r\n\r\nNorint išvengti koronaviruso patariama laikytis bendrųjų higienos taisyklių: plauti rankas su muilu ir šiltu vandeniu, dezinfekuoti rankas, \r\nlaikytis čiaudėjimo ir kosėjimo etiketo, ne mažiau, kaip 2–3 kartus per dieną vėdinti patalpas, kasdien valyti dažnai naudojamus paviršius kambaryje \r\n(pvz., naktinius stalelius, lovų rėmus ir kitus miegamojo baldus), naudoti apsaugos priemones, vengti būriavimosi, nuo kitų asmenų išlaikant bent 2 metrų atstumą.\r\n\r\nĮtariamu koronaviruso atveju laikomas: \r\n\r\n1) asmuo su ūmia kvėpavimo takų infekcija (staigiai prasidėjęs bent vienas iš šių simptomų: karščiavimas, kosulys, apsunkintas kvėpavimas) \r\nir 14 dienų laikotarpiu iki simptomų pradžios keliavo ar gyveno teritorijose, kur vyksta COVID-19 ligos (koronaviruso infekcijos) protrūkiai ar plitimas visuomenėje;\r\n\r\n2) asmuo su ūmios kvėpavimo takų infekcijos simptomais ir buvęs artimame sąlytyje su patvirtintu ar tikėtinu COVID-19 ligos (koronaviruso infekcijos) \r\natveju 14 dienų laikotarpiu iki simptomų pradžios;\r\n\r\n3) asmuo su sunkia ūmia kvėpavimo takų infekcija (karščiavimas ir bent vienas kvėpavimo takų ligos simptomų – kosulys, apsunkintas kvėpavimas) \r\nir kuriam būtina hospitalizacija ir nėra nustatyta kitos etiologinės priežasties, kuri paaiškintų šiuos simptomus.', 1, 4, 1, 'https://www.tv3.lt/Uploads/UArticles/leadPhotos/f6/16/b9/1a/f616b91a0563e3a0a21974f961378208.jpg', 1589820449, 1589820449),
(9, 'Koronaviruso akivaizdoje išaugo Naujosios Zelandijos premjerės populiarumas', 'zelandijos-premjeres-populiarumas', 'Koronaviruso pandemijos akivaizdoje Jacinda Ardern tapo populiariausia Naujosios Zelandijos premjere per visą amžių, rodo „Newshub-Reid Research“ apklausos rezultatai, skelbia agentūra „Reuters“.', 'Sėkmingos J. Ardern pastangos, siekiant suvaldyti naujojo koronaviruso protrūkį, lėmė didelį jos populiarumo šuolį. Naujoji Zelandija patenka tarp šalių, kurioms geriausiai pavyko pažaboti ligos plitimą.\r\n\r\nNaujojoje Zelandijoje patvirtintų koronaviruso infekcijos atvejų skaičius siekia 1 499, mirė 21 pacientas. Visame pasaulyje virusas diagnozuotas daugiau kaip 4,7 mln. žmonių, daugiau kaip 315 tūkst. iš jų mirė.\r\n\r\n#imgabs https://www.lrt.lt/img/2019/11/28/558292-150145-1287x836.jpg\r\n\r\nNaujosios Zelandijos premjerės ir jos partijos populiarumas pasiekė rekordines aukštumas. Remiantis „Newshub-Reid Research“ apklausos rezultatais, J. Ardern Leiboristų partijos populiarumas smarkiai išaugo – 14 proc. punktų iki 56,5 proc., o palankumas pačios premjerės atžvilgiu išaugo 20,8 proc. punkto iki 59,5 proc.\r\n\r\nTuo metu didžiausios partijos Naujosios Zelandijos parlamente – Nacionalinės partijos – populiarumas smuko 12,7 proc. punkto, iki 30,6 proc.\r\n\r\n#imgabs https://www.lrt.lt/img/2020/05/18/655497-249506-1287x836.jpg', 1, 4, 1, 'https://www.lrt.lt/img/2020/05/18/655496-897881-1287x836.jpg', 1589821297, 1589821297),
(10, 'Senovinis meteoritas yra pirmasis įrodymas, kad Marsas – vulkaniškai aktyvus', 'senovinis-meteoritas', 'Daugelį metų manėme, kad Marsas yra miręs. Dulkėta, sausa ir tuščia planeta, kur vienintelis judėjimas yra vėjo nešamos dulkės. Tačiau pastaruoju metus pasirodo vis daugiau įrodymų, kad Marsas yra vulkaniškai ir geologiškai aktyvus, rašo „Science Alert“.', 'Vulkaniškai aktyvaus Marso idėja šiomis dienomis tapo kur kas realesnė. Mokslininkų teigimu, giliai Marso „viduriuose“ susiformavęs meteoritas pateikė pirmuosius nenuneigiamus cheminius magmos konvekcijos Marso mantijoje įrodymus.\r\n\r\nOlivino kristalai 2011 m. į Žemę nukritusiame Tissinto meteorite galėjo susiformuoti tik besikeičiančioje temperatūroje, tik jam sukantis magmos konvekcijos srovėse, o tai rodo, kad maždaug prieš 574-582 mln. metų, kai šis meteoritas formavosi, planeta buvo vulkaniškai aktyvi, su petrūkiais galėjo tokia išlikti ir iki šių dienų.\r\n\r\n„Anksčiau neturėjome jokių vulkaninės konvekcijos Marse įrodymų, tačiau į klausimą, ar Marsas vis dar yra vulkaniškai aktyvi planeta, buvo bandoma atsakyti, pasitelkiant įvairius tyrimo metodus, – „Science Alert“ aiškino Glazgo universiteto planetų geologas Nicola Mari. – Visgi tai yra pirmasis tyrimas, kurio metu pavyko įrodyti aktyvumą Marso giluminiuose sluoksniuose grynai iš cheminės perspektyvos, naudojant tikrus mėginius iš Marso.“\r\n\r\n#imgabs https://www.lrt.lt/img/2020/03/15/621195-409770-1287x836.jpg\r\n\r\nOlivinas – magnio ir geležies silikatas – nėra retas. Jis kristalizuojasi vėstant magmai ir Žemės mantijoje sutinkamas dažnai; tiesą pasakius, olivino grupės mineralai vyrauja Žemės mantijoje, dažnai sudarydami uolienų masę. Žemės paviršiuje jis randamas vulkaninės kilmės uolienose.\r\n\r\nOlivino gana dažnai pasitaiko ir meteorituose, jis nėra retas ir Marso paviršiuje. Tiesą sakant, tai, kad Marso paviršiuje yra olivino, buvo laikoma įrodymu, jog Raudonojoje planetoje nėra vandens, nes drėgmėje šis mineralas greitai eroduoja.\r\n\r\nTačiau kai N. Mari suburta mokslininkų grupė ėmėsi tirti Tissinto meteorite esančius olivino kristalus, kad daugiau sužinotų apie magmos židinį, kuriame jis susiformavo, jie pastebėjo kai ką neįprasto. Kristalas pasižymėjo netaisyklingais fosforu prisotintais ryšiais.\r\n\r\nŽinome apie šį reiškinį Žemėje, jis vadinamas tirpinio akumuliacija. Tačiau aptikti jį Marse buvo labai netikėta.\r\n\r\n„Tai įvyksta tuomet, kai kristalai auga greičiau nei fosforas gali išsisklaidyti tirpdamas, todėl jam nelieka nieko kito kaip tik prasiskverbti į kristalo struktūrą vietoje to, kad „plauktų“ skystoje magmoje, – aiškino N. Mari. – Magmos židinyje, kur susidarė mano tirta lava, konvekcija buvo tokia galinga, kad olivinai iš židinio apačios (karštesnės aplinkos) į viršų (vėsesnę aplinką) buvo išstumti labai greitai, tiksliau kalbant, tokiomis sąlygomis olivinai vėso maždaug 15-30 °C per valandą greičiu.“\r\n\r\n#imgabs https://www.lrt.lt/img/2019/05/15/429319-774520-1287x836.jpg\r\n\r\nDidesnieji olivino kristalai taip pat suteikė nemažai informacijos. Nikelio ir kobalto pėdsakai patvirtino ankstesnes išvadas, kad jie yra susidarę žemiau Marso plutos, maždaug 40-80 km gylyje.\r\n\r\nMokslininkai nustatė, kad Vėlyvajame amazonės periode, kai formavosi olivino kristalai, Marso mantijos temperatūra galėjo siekti 1560 laipsnių Celsijaus. Tai labai primena Žemės mantijos temperatūrą, kuri Archėjaus eone, t. y. prieš 4-2,5 mlrd. metų, siekė 1 650 laipsnių Celsijaus.\r\n\r\nTai nereiškia, kad Marsas primena Žemę ankstyvoje formavimosi stadijoje. Tačiau tai reiškia, kad Marsas gali būti išlaikęs šiek tiek karščio po savo mantija; manoma, kad neturėdamas plokščių tektonikos, kuri Žemėje padeda išsklaidyti karštį, Marsas galbūt kur kas lėčiau vėsta.', 1, 3, 1, 'https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fwww.universetoday.com%2Fwp-content%2Fuploads%2F2016%2F05%2FMars-Hubble-May-2016_UT.jpg&f=1&nofb=1', 1589833658, 1589833658),
(11, 'Prancūzija ir Vokietija siūlo sukurti 500 mlrd. eurų fondą ES ekonomikos atkūrimui', 'pranczija-ir-vokietija-siulo-sukurti-500-mlrd-eur-fonda-es-ekonomikos-atkrimui', 'Prancūzija ir Vokietija pirmadienį pasiūlė sukurti 500 mlrd. eurų fondą atkurti Europos Sąjungos ekonomikai, kuri dėl koronaviruso pandemijos išgyvena didžiausią pokario krizę.', '#i „Siekdami palaikyti tvarų ekonomikos atsigavimą, kuris atkurtų ir sustiprintų augimą ES, Vokietija ir Prancūzija palaiko ambicingo, laikino ir tikslingo atkūrimo fondo sukūrimą“,\r\n– sakoma bendrame pranešime ir priduriama, kad jis bus iš dalies finansuojamas „skolinantis iš rinkos ES vardu“.\r\n\r\n#i „Tai (koronaviruso pandemija), ir čia mes vieningi, yra didžiausia krizė ES istorijoje. Tokia krizė reikalauja atitinkamo atsako“,\r\n– pabrėžė Vokietijos kanclerė Angela Merkel.\r\n\r\nPasak kanclerės, Berlynas ir Paryžius nori veikti kartu su kitomis ES šalimis, bet savo iniciatyva ketina pasiųsti joms signalą, kad Bendrija įveiks krizę ir išeis iš jos dar stipresnė.\r\n\r\n#i „Mes nusprendėme, kad reikalingas (ekonomikos) atkūrimo fondas. Dėl to, kaip šis fondas gali atrodyti, Vokietija ir Prancūzija aktyviai diskutavo“,\r\n– pridūrė A. Merkel.\r\n#i „Svarbu greitai atkurti ekonomiką. Todėl mes norime įsteigti riboto termino 500 milijardų eurų fondą, kuris turi teikti ES biudžeto lėšas labiausiai nukentėjusiems sektoriams ir regionams“, – pareiškė Vokietijos kanclerė.\r\n\r\n#imgabs https://www.lrt.lt/img/2020/02/20/606195-154591-1287x836.jpg\r\n\r\nPrancūzijos prezidentas Emmanuelis Macronas pirmadienį taip pat pareiškė, kad stipresnė Europos koordinacija sveikatos klausimais privalo tapti prioritetu, ir pripažino, kad pirminio Bendrijos atsako į koronaviruso protrūkį nepakako.\r\n\r\n#imgabs https://www.lrt.lt/img/2020/03/31/629333-475864-1287x836.jpg\r\n\r\nE. Macronas bendroje nuotolinėje spaudos konferencijoje su Vokietijos kanclere Angela Merkel sakė, kad kai kurių ES šalių vienašališki žingsniai uždaryti sienas sudarė „liūdną Europos įvaizdį“.\r\n\r\n#i „Europos sveikata turi būti mūsų prioritetas“,\r\n– teigė Prancūzijos lyderis.\r\n\r\n#imgabs https://www.lrt.lt/img/2020/04/16/637422-478933-1287x836.jpg\r\n\r\nEuropos Komisijos vadovė Ursula von der Leyen savo ruožtu pagyrė Berlyno ir Paryžiaus pasiūlytą planą.\r\n\r\n#i „Sveikinu konstruktyvų Prancūzijos ir Vokietijos pasiūlymą. Jame pripažįstamas ekonominių iššūkių, su kuriais susiduria Europa, mastas ir dydis“,\r\n– sakė EK, kuriai tektų įgyvendinti šį planą, pirmininkė.', 1, 5, 1, 'https://www.lrt.lt/img/2019/03/07/362662-421812-1287x836.jpg', 1589835118, 1589835118),
(12, 'InfoDelfinai projektas', 'infodelfinai-projektas', 'InfoDelfinai yra naujienu portalas veikiantis ant Laravel karkaso.', 'Svetainę sudaro 5 puslapiai:\r\n\r\n#ol\r\n#li Pagrindinis puslapis ir kategorijos: įvairūs straipsniai (atitinka index.html)\r\n#li Prisijungti/atsijungti (atitinka forma.html)\r\n#li Straipsniai (atitinka galerija.html, nes straipsniai turi savo nuotraukas)\r\n#li Straipsnių valdymas: galima pridėti, redaguoti, pašalinti straipsnius (atitinka forma.html, nes siunčiama forma redaguojant ir pridedant straipsnius)\r\n#li Kategorijų valdymas: galima pridėti, redaguoti, pašalinti kategorijas (atitinka forma.html, nes siunčiama forma redaguojant ir pridedant kategorijas)\r\n#/ol\r\n\r\nJoje naudojami semantiniai elementai, kaip <nav>, <aside>, <article>, <footer> ir kiti.\r\n\r\n#imgabs https://cdn.discordapp.com/attachments/493500446573461525/712009378345254942/img_sem_elements.png\r\n\r\nTaip pat viršuje matomas logotipas ir navigacija kurioje galima lengvai pasiekti kategorijų ir straipsnių valdymą.\r\n\r\n#imgabs https://i.imgur.com/ZvoaJoq.png\r\n\r\nKai naršoma kategorijose ir straipsniuose, matoma navigacija dešinėje kurioje galima matyti visas kategorijas.\r\n\r\nPereinant tarp puslapių keičiasi tik turinio dalis.\r\n\r\nSvetainė yra pilnai pritaikyta telefonų ir kompiuterių ekranams.\r\n\r\nKategorijose visi straipsniai išdėstyti \"mūriniu\" būdu naudojant \"Colcade\" JavaScript paketą:\r\n\r\n#imgabs https://cdn.discordapp.com/attachments/493500446573461525/712068408824561744/masonry.png', 1, 3, 1, 'https://i.imgur.com/5N3rdJl.png', 1589835509, 1589835509);

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `url` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(191) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `url`, `description`) VALUES
(3, 'Mokslas', 'mokslas', 'Viskas apie moksla ir daugiau!'),
(4, 'COVID-19', 'covid19', 'Coronavirusas'),
(5, 'Ekonomika', 'ekonomika', 'Viso pasaulio ekonomikos naujienos');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8_unicode_ci NOT NULL,
  `queue` text COLLATE utf8_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2020_05_10_110502_create_categories', 1),
(5, '2020_05_11_095905_create_articles', 1),
(6, '2020_05_15_172742_add_user_roles', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `role` varchar(191) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'member'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`, `role`) VALUES
(1, 'admin', 'admin@admin.com', NULL, '$2y$10$VbgeLLHr8XpjPKaimrUrSemnZIqBciM9LFB5QqtvQbj.fGexI2.SW', '9jrz8fGQttijAvx1kTQwZbSsNSJ5Fr380Nfuyy7rZLNaJe5jtLdW6rjLr05N', '2020-05-17 12:50:53', '2020-05-17 12:50:53', 'admin');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `articles`
--
ALTER TABLE `articles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `articles_url_unique` (`url`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `categories_url_unique` (`url`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `articles`
--
ALTER TABLE `articles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

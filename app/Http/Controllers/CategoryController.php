<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request) {
        //
    }



    public function index(Request $request) {
        return CategoryController::showCategory($request, "main");
    }

    public static function showCategory(Request $request, $id) {
        $cur = DB::select('select * from categories where url=? limit 1',[$id]);
        if(!isset($cur[0]))abort(404,"Kategorija nerasta");
        $cur = $cur[0];
        $render = "<div class='rgrid'>
        <div class='rgrid-col'></div>
        <div class='rgrid-col'></div>
        <div class='rgrid-col'></div>
        <div class='rgrid-col'></div>";
        $art = CategoryController::articles($cur->id);
        foreach($art as $val){
            $render .= ArticleController::genCard($val);
        }
        $render .= '</div>';
        return view('category', ['current' => $cur,
            'articles' => $art,
            'list' => $render,
            'categories' => DB::select('select * from categories ORDER BY name')]);
    }

    public static function showPopular(Request $request) {
        $render = "<div class='rgrid'>
        <div class='rgrid-col'></div>
        <div class='rgrid-col'></div>
        <div class='rgrid-col'></div>
        <div class='rgrid-col'></div>";
        $art = DB::select('select * from articles where public=1 order by created_at desc limit 50');
        foreach($art as $val){
            $render .= ArticleController::genCard($val);
        }
        $render .= '</div>';
        return view('home', [
            'articles' => $art,
            'list' => $render,
            'categories' => DB::select('select * from categories ORDER BY name')
            ]);
    }

    public static function settings(Request $request){
        if(isset($request->id)){
            $dbb = DB::select('select * from categories where id=?',[$request->id]);
            if(isset($dbb[0])){
                return view('category-settings', ['categories' => DB::select('select * from categories'), 'editcat' => $dbb[0]]);
            }else{
                abort(405, "Kategorija nerasta");
            }
        }else{
            return view('category-settings', ['categories' => DB::select('select * from categories')]);
        }
    }



    public static function info($id) {
        $info = DB::select('select * from categories where url = ?', [$id]);
        return $info;
    }

    public static function articles($id){
        return DB::select('select * from articles where category=? AND public=1 order by created_at desc', [$id]);
    }

    public static function addCategory(Request $request){
        $url = strtolower(preg_replace("/[^A-Za-z0-9]/", "", $request->name));
        DB::insert('insert into categories (url, name, description) values (?, ?, ?)', [$url, $request->name, $request->desc]);
        return redirect()->route('category-settings');
    }

    public static function updateCategory(Request $request){
        DB::update('update categories set url=?, name=?, description=? where id=?', [$request->url, $request->name, $request->desc, $request->id]);
        return redirect()->route('category-settings');
    }

    public static function deleteCategory(Request $request){
        DB::delete('delete from categories where id=?', [$request->id]);
        return redirect()->route('category-settings');
    }
}

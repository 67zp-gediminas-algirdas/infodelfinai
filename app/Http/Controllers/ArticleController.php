<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Auth;

class ArticleController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        //
    }

    public static function settings(Request $request){
        $articles = DB::select('SELECT articles.*, users.name FROM articles INNER JOIN users ON articles.author=users.id order by created_at desc  ');
        if(isset($request->id)){
            $dbb = DB::select('SELECT articles.*, users.name FROM articles INNER JOIN users ON articles.author=users.id where articles.id=?  order by created_at desc ',[$request->id]);
            if(isset($dbb[0])){
                foreach ($articles as &$value) {
                    if($value->category!=-1){
                        $value->categoryName=DB::select('SELECT * FROM categories WHERE id=?',[$value->category])[0]->name;
                    }else{
                        $value->categoryName="---";
                    }
                }
                return view('article-settings', ['articles' => $articles, 'editcat' => $dbb[0],
                'categories' => DB::select('select * from categories ORDER BY name')]);
            }else{
                abort(405, "Straipsnis nerastas");
            }
        }else{
            foreach ($articles as &$value) {
                if($value->category!=-1){
                    $value->categoryName=DB::select('SELECT * FROM categories WHERE id=?',[$value->category])[0]->name;
                }else{
                    $value->categoryName="---";
                }
            }
            return view('article-settings', ['articles' => $articles,
            'categories' => DB::select('select * from categories ORDER BY name')]);
        }
    }

    public static function showArticle(Request $request, $id) {
        $cur = DB::select('SELECT articles.*, users.name FROM articles INNER JOIN users ON articles.author=users.id where url = ? order by created_at desc  limit 1',[$id]);
        if(!isset($cur[0]))abort(404,"Straipsnis nerastas");
        $cur = $cur[0];
        $curAuthor = $cur->name;
        if(!isset($curAuthor)){
            $curAuthor = 'Anonimas';
        }

        $con = preg_split("/\r\n|\n|\r/", $cur->content);
        $str = "";
        foreach ($con as $key => $value){
            if(substr($value, 0, 5) == '#img '){
                $str .= "<img class='img' src='".url("/img/s/".htmlspecialchars(substr($value, 5)))."'>";
            }elseif(substr($value, 0, 8) == '#imgabs '){
                $str .= "<img class='img' src='".htmlspecialchars(substr($value, 8))."'>";
            }elseif(substr($value, 0, 4) == '#h1 '){
                $str .= "<h2 class='mb-0'>".htmlspecialchars(substr($value, 4))."</h2>";
            }elseif(substr($value, 0, 4) == '#h2 '){
                $str .= "<h3 class='mb-0'>".htmlspecialchars(substr($value, 4))."</h3>";
            }elseif(substr($value, 0, 4) == '#h3 '){
                $str .= "<h4 class='mb-0'>".htmlspecialchars(substr($value, 4))."</h4>";
            }elseif(substr($value, 0, 4) == '#li '){
                $str .= "<li>".htmlspecialchars(substr($value, 4))."</li>";
            }elseif(substr($value, 0, 3) == '#ul'){
                $str .= "<ul>";
            }elseif(substr($value, 0, 4) == '#/ul'){
                $str .= "</ul>";
            }elseif(substr($value, 0, 3) == '#ol'){
                $str .= "<ol>";
            }elseif(substr($value, 0, 4) == '#/ol'){
                $str .= "</ol>";
            }elseif(substr($value, 0, 3) == '#i '){
                $str .= "<i>".htmlspecialchars(substr($value, 3))."</i>";
            }elseif(substr($value, 0, 3) == '#b '){
                $str .= "<b>".htmlspecialchars(substr($value, 3))."</b>";
            }elseif(substr($value, 0, 9) == '#linkurl '){
                $str .= "<a href='".htmlspecialchars(substr($value, 9))."'>";
            }elseif(substr($value, 0, 9) == '#linktxt '){
                $str .= htmlspecialchars(substr($value, 9))."</a>";
            }elseif(substr($value, 0, 3) == '#hr'){
                $str .= "<hr>";
            }elseif($value == ''){
                $str .= "<br><br>";
            }else{
                $str .= htmlspecialchars($value);
            }
            $str .= ' ';
        }
        

        return view('article', ['current' => $cur,
            'straipsnis' => $str,
            'current' => $cur,
            'imgUrl' => $cur->thumbnail?$cur->thumbnail:NULL,
            'time' => date("Y-m-d H:i",$cur->created_at),
            'currentAuthor' => $curAuthor,
            'articles' => CategoryController::articles($id),
            'categories' => DB::select('select * from categories ORDER BY name')]);
    }

    public static function genCard($cur){
        return "
        <div class='around-space rgrid-item'>
            <a href='".url("/s/".htmlspecialchars($cur->url))."'>
            <div class='card card-article'>
                <img class='card-img-top' src='".htmlspecialchars($cur->thumbnail)."'>
                <div class='card-header'>".htmlspecialchars($cur->title)."</div>
                <div class='card-body' style='color:#000'>".htmlspecialchars($cur->shortDescr)."</div>
            </div>
            </a>
        </div>
        ";
    }

    public static function addArticle(Request $request){
        $url = strtolower(preg_replace("/[^A-Za-z0-9]/", "", $request->name));
        DB::insert('insert into articles '.
        '(created_at, updated_at, url,  title,          shortDescr,     content, author,           public, thumbnail, category) values '.
        '(?,          ?,          ?,    ?,              ?,              ?,       ?,                ?,      ?,         ?)',
        [ time(),     time(),     $url, $request->name, $request->desc, "",      Auth::user()->id, false,  '',        -1]);
        return redirect()->route('article-modify', ['id'=>DB::select('SELECT LAST_INSERT_ID() AS a')[0]->a]);
    }

    public static function updateArticle(Request $request){
        DB::update('update articles set url=?, title=?, shortDescr=?, public=?, content=?, category=? where id=?', [$request->url, $request->name, $request->desc, isset($request->public)?1:0, $request->straipsnis??'', $request->category, $request->id]);
        return redirect()->route('article-settings');
    }

    public static function deleteArticle(Request $request){
        DB::delete('delete from articles where id=?', [$request->id]);
        return redirect()->route('article-settings');
    }
}

<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;

use Illuminate\Support\Facades\Gate;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
    
        // patikrina ar administratorius
        Gate::define('accessAdminpanel', function($user) {
            return $user->role(['superadmin', 'admin']);
        });

        // patikrina ar autorius
        Gate::define('accessAuthor', function($user) {
            return $user->role(['superadmin','admin','author']);
        });

        // patikrina ar moderatorius
        Gate::define('accessModerator', function($user) {
            return $user->role(['superadmin','admin','moderator']);
        });
    
        // patikrina ar vartotojas
        Gate::define('accessProfile', function($user) {
            return $user->role(['superadmin','admin','member', 'author', 'moderator']);
        });
        
        Schema::defaultStringLength(191);
    }
}
